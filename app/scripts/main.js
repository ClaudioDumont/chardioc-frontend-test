jQuery(function ($) {

    LAYOUTSHOP = window.LAYOUTSHOP || {};
    LAYOUTSHOP.options = {};

    LAYOUTSHOP.carousel = function () {
        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            nav: true,
            navText: ["<i class='fa fa-chevron-left fa-2x'></i>", "<i class='fa fa-chevron-right fa-2x'></i>"],
            autoplay: true,
            autoplayHoverPause: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 3
                }
            }
        });
    };
   
    LAYOUTSHOP.rating = function (){
        $('.rating-tooltip-manual').rating();
    };

    $(document).ready(function () {
        LAYOUTSHOP.carousel();
        LAYOUTSHOP.rating();
    });

});